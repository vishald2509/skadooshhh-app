export function setCourse(course) {
  return {
    type: "SET_COURSE",
    payload: course
  };
}
