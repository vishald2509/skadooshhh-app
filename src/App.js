import React from 'react';
import {Switch, Route} from 'react-router-dom';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navbar from './components/Navbar'
import MenuView from './components/MenuView'
import CourseView from './components/CourseView'
import Modal from './components/Modal'
import Default from './components/Default'



function App() {
  return (
    <React.Fragment>
      <Navbar/>
      <Switch>
        <Route exact path="/" component={MenuView}/>
        <Route path="/courseview" component={CourseView}/>
        <Route component={Default}/>
      </Switch>
      <Modal/>
    </React.Fragment>
  );
}

export default App;
