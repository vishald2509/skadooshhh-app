import vegimage from '../veg.png';
import nonvegimage from '../nonveg.png';
import desertimg from '../desertimg.jpg';
import maincourseimg from '../maincourseimg.jpg';
import starterimg from '../starterimg.jpg';
import beverageimg from '../beverageimg.jpg';



const menuReducer = (state = {
    veg: vegimage,
    nonveg: nonvegimage,
    beverageimg: beverageimg,
    starterimg: starterimg,
    desertimg: desertimg,
    maincourseimg: maincourseimg
  }, action) => {
    return state;
};

export default menuReducer;
