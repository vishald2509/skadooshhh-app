const courseReducer = (state = {
    course: ""
}, action) => {
switch(action.type){
  case "SET_COURSE":
    state = {
      ...state,
      course: action.payload.toLowerCase().replace(/ +/g, "")
    };
    break;

  default:
  break;
}
return state;
};

export default courseReducer;
