import styled from 'styled-components';


export const ButtonContainer = styled.button`
  text-transform: capitalize;
  font-size:1.4rem;
  background: transparent;
  border:0.05rem solid var(--lightBlue);
  color: var(--lightBlue);
  border-radius: 0.5rem;
  padding: 0.2rem 0.5rem;
  cursor:pointer;
  margin: 0.2rem 0.5rem 0.2rem 0;
  transition:all 0.5s ease-in-out;
  &:hover{
    color: var(--mainBlue);
  }
  &:focus{
    outline: none;
  }
`;


export const SearchField = styled.input`
  opacity: 0.3;
  font-weight: 600;
  font-size: 0.9rem;
  background: transparent;
  border:transparent;
  width: 180px;
  color: var(--mainDark);
  border-radius: 0.5rem;
  &:focus{
    outline: none;
  }
`;

export const Add = styled.button`
  border : solid 1px rgba(0,0,0,0.2);
  font-size: 20px;
  float: right;
  margin-right: 50px;
  background: transparent;
  border-radius: 0.4rem;
  color: var(--lightBlue);
  width: 100px;
`;
