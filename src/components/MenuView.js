import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {SpecialMenuItem, CourseMenuItem} from "./Items"
import { setCourse } from '../actions/courseActions';

class MenuView extends Component {
  constuctor() {
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(courseName) {
    this.props.setCourse(courseName);
    let path = `/courseview`;
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="container-fluid p-0">
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-12 mb-3 mt-4 p-0">
                <label className="text-title">Todays Special</label>
                <label className="title-link">See All</label>
              </div>
            </div>
            <div className="row mx-auto text-center">
              <SpecialMenuItem imageUrl={this.props.menu.maincourseimg} alt="SpecialItem1" foodtype={this.props.menu.veg} name="Kofta Kurma"/>
              <SpecialMenuItem imageUrl={this.props.menu.starterimg} alt="SpecialItem2" foodtype={this.props.menu.nonveg} name="Veg Momo"/>
              <SpecialMenuItem imageUrl={this.props.menu.beverageimg} alt="SpecialItem3" foodtype={this.props.menu.veg} name="Kadai Paneer"/>
              <SpecialMenuItem imageUrl={this.props.menu.desertimg} alt="SpecialItem4" foodtype={this.props.menu.nonveg} name="Pastery"/>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-12 mb-3 mt-4 p-0">
                <label className="text-title">courses</label>
              </div>
            </div>
            <div className="row mx-auto text-center">
              <CourseMenuItem imageUrl={this.props.menu.starterimg} alt="StartersImage" courseName="Starters" changeCourse={(courseName)=>this.routeChange(courseName)} />
              <CourseMenuItem imageUrl={this.props.menu.maincourseimg} alt="MainCourseImage" courseName="Main Course" changeCourse={(courseName)=>this.routeChange(courseName)}/>
            </div>
            <div className="row mx-auto text-center">
              <CourseMenuItem imageUrl={this.props.menu.desertimg} alt="DesertsImage" courseName="Deserts" changeCourse={(courseName)=>this.routeChange(courseName)}/>
              <CourseMenuItem imageUrl={this.props.menu.beverageimg} alt="BeveragesImage" courseName="Beverages" changeCourse={(courseName)=>this.routeChange(courseName)}/>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    menu: state.menu,
    course: state.course
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCourse: (course) => {
      dispatch(setCourse(course));
    },
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MenuView));
