import React from 'react';
import styled from 'styled-components';
import {Add} from './Button';

export const SpecialMenuItem = ({imageUrl, alt, foodtype, name}) => (
  <div className="col-3 p-0">
    <div className="row">
      <div className="col-12 p-0">
        <SpecialItemImage className="d-inline" src={imageUrl} alt={alt} />
        <FoodType src={foodtype} alt="FoodType" specialItem />
      </div>
    </div>
    <div className="row">
      <div className="col-12 p-0">
        <label htmlFor="SpecialItemName">{name}</label>
      </div>
    </div>
  </div>
);

export const CourseMenuItem = ({changeCourse, imageUrl, alt, courseName}) => (
  <CourseMenuItemWrapper className="col-5 p-0 d-inline-flex" onClick={() => changeCourse(courseName)}>
    <CourseMenuItemImage src={imageUrl} alt={alt}/>
    <label htmlFor="">{courseName}</label>
  </CourseMenuItemWrapper>
);

export const CourseItem = ({imageUrl, itemname, foodtype, price, cusine, rating, noofrating }) => (
  <CourseItemWrapper className="row">
    <div className="col-5"><div className="d-inline-flex imagewrapper"><CourseMenuItemImage className="course-menu-item-image" src={imageUrl}/></div></div>
    <div className="col">
      <div className="row">
        <div className="col-12 p-0">
          <FoodType src={foodtype} alt="FoodType" className="food-type"/>
          <label htmlFor="itemName" className="item-name">{itemname}</label>
        </div>
      </div>
      <div className="row">
        <div className="col-12 p-0">
          <i className="fas fa-star ratingstar" ></i>
          <label className="itemRating" htmlFor="itemRating">{rating}</label>
          <label className="noofrating" htmlFor="noofrating">{noofrating} Ratings</label>
        </div>
      </div>
      <div className="row">
        <div className="col-12 p-0">
          <label htmlFor="cusine" className="cusine">{cusine}</label>
        </div>
      </div>
      <div className="row">
        <div className="col-12 p-0">
          <label htmlFor="itemprice" className="itemprice">
            <i className="fas fa-rupee-sign"></i>{price}
          </label>
          <Add>
            <i className="fas fa-plus plus-icon"></i> Add
          </Add>
        </div>
      </div>
    </div>
  </CourseItemWrapper>
);

const SpecialItemImage = styled.img`
  height: 80px;
  width: 80px;
  border-radius: 40px;
`;
const CourseMenuItemImage = styled.img`
  height: 60px;
  margin: 5px;
  border-radius: 10px;
`;
const CourseMenuItemWrapper = styled.div`
  border-radius: 10px;
  box-shadow: 0px 3px 5px 3px rgba(0, 0, 0, .2);
  margin: 15px;
  vertical-align: center;
  label{
    line-height:60px;
    padding-left: 10px;
  }
`;


const FoodType = styled.img`
  height: ${prop => prop.specialItem ? "10px" : "25px"};
  width: ${prop => prop.specialItem ? "10px" : "25px"};
  margin-left: ${prop => prop.specialItem ? "-15px" : "0px"};
`;

const CourseItemWrapper = styled.div`
  .imagewrapper{
    border-radius: 10px;
    box-shadow: 0px 3px 5px 3px rgba(0, 0, 0, .2);
  }
  .course-menu-item-image{
    height: 155px;
    width: 180px;
  }
  .food-type{
    height: 25px;
    margin-top: -10px;
  }
  .item-name{
    padding-left: 10px;
    font-weight: bolder;
    font-size: 25px;
  }
  .ratingstar{
    color: #f0e754;
    padding-top: 2px;
    font-size: 18px;
    padding-right: 10px;
  }
  .itemRating{
    opacity: 0.5;
    font-weight: bolder;
    font-size: 20px;
    padding-right: 10px;
  }
  .noofrating{
    opacity: 0.5;
    font-size: 18px;
  }
  .cusine{
    font-size: 18px;
    opacity: 0.5;
    border: 1px solid rgba(0, 0, 0, .5);
    border-radius: 15px;
    padding: 0px 6px 0px 6px;
  }
  .itemprice{
    font-size: 20px;
    font-weight: bolder;
    opacity: 0.7;
  }
`;
