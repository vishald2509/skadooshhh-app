import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import profileid from '../profile.png';
import { SearchField } from './Button'
import SwitchButton from './SwitchButton'

class Navbar extends Component {

  render() {
    return (
      <React-Fragment>
      <NavWrapper className="navbar navbar-expand-sm navbar-default">

        <ul className="navbar-nav align-items-center">
          <li className="nav-item ml-5 pt-3">
            <Link to="/" className="nav-link home-link">
              SKADOOSHHH
            </Link>
          </li>
      </ul>
      <Link to="/" className="ml-auto">
          <span className="mr-2">
            <img src={profileid} alt="profileimage"/>
          </span>
      </Link>
      </NavWrapper>
      <BreadcrumbWrapper>
        <ol className="breadcrumb">
          <li className="breadcrumb-item "><SwitchLabel>ONLY VEG</SwitchLabel><SwitchButton className="switch-button"/></li>
          <li className="breadcrumb-sceperator"></li>
          <li className="breadcrumb-item">
            <SearchField className="search-field" type="text" placeholder="Search for Food, cusines"/>
            <span><i className="search-icon fas fa-search"></i></span>
          </li>
        </ol>
      </BreadcrumbWrapper>
      </React-Fragment>

    );
  }

}

const NavWrapper = styled.nav`
  background: var(--mainWhite);
  .nav-link{
    color:var(--mainDark)!important;
    font-size:1.3rem;
    text-transform: capitalize;
  }
  .home-link{
    opacity: 0.3;
    font-size: 26px;
    font-weight: bolder;
    font-family: Arial, Helvetica, sans-serif;
  }
  img{
    height: 50px;
    border-radius: 25px;
  }
`;
const BreadcrumbWrapper = styled.nav`
  .breadcrumb-sceperator::before {
    content: "|";
    padding-right: 2rem;
    padding-left: 2rem;
  }
  .breadcrumb {
    display: block;
    text-align: center;
  }
  .breadcrumb-item {
    text-align: center;
    display: inline;
  }
  li{
    float: none !important;
    display: inline-block;
  }
  line-height:50px;
  ol{
    background: var(--mainWhite) !important;
  }
`;

const SwitchLabel = styled.label`
  font-weight: 700;
  opacity: 0.4;
  padding-right: 20px;
  padding-left: 20px;
`


export default Navbar;
