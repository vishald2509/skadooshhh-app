import React, { Component } from 'react';
import {CourseItem} from './Items';
import { connect } from 'react-redux';
import { setCourse } from '../actions/courseActions';

import chickenlolipop from '../chickenlolipop.jpg';

class CourseView extends Component {

  render() {
    return (
      <div className="container-fluid p-0">
        <div className="row">
          <div className="col-12 mb-3 mt-4 p-0 pl-5">
            <i className="fas fa-arrow-left back-icon"></i>
            <label className="text-title">{this.props.course.course}</label>
          </div>
        </div>
        <div className="row">
          <div className="col-12 ml-3">
            <CourseItem imageUrl={chickenlolipop} itemname="Chicken Lolipop" foodtype={this.props.menu.nonveg} price="250" cusine="Chinese" rating="4.2" noofrating="230" />
          </div>
        </div>
      </div>
    );
  }

}


const mapStateToProps = (state) => {
  return {
    menu: state.menu,
    course: state.course
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCourse: (course) => {
      dispatch(setCourse(course));
    },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(CourseView);
