import React, { Component } from 'react';
import Switch from "react-switch";

class SwitchButton extends Component {
  constructor() {
    super();
    this.state = { checked: false };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(checked) {
    this.setState({ checked });
  }

  render() {
    return (
      <label>
        <span></span>
        <Switch onChange={this.handleChange} checked={this.state.checked} handleDiameter={25} height={20} width={45} offHandleColor="#ebe6e6" boxShadow="-2px 5px 2px 1px rgba(0, 0, 0, .2)" checkedIcon={false} uncheckedIcon={false} onColor="#4ceb34" offColor="#cfcccc"/>
      </label>
    );
  }

}

export default SwitchButton;
