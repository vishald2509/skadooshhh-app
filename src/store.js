import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";

import menu from './reducers/menuReducer';
import course from './reducers/courseReducer';


export default createStore(
  combineReducers({menu, course}),
  { },
  applyMiddleware(logger)
);
